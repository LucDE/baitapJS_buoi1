
/**Bài 1
 * Input: Số ngày làm, lương một ngày
 * 
 * Step: 
 * s1: tạo biến số ngày làm
 * s2: tạo biến lương một ngày = 100.000
 * s3: Tiền lương = Số ngày làm * Lương một ngày
 * 
 * Output: Tiền lương 
 */
console.log("Bài 1:");
var work_day = 26;
var salary_per_day = 100000;
var total_salary ;
total_salary = salary_per_day * work_day;
console.log("Tiền lương: " + total_salary + " VNĐ");

/**Bài 2
 * Input: 5 số thực
 * 
 * Step: 
 * s1: tạo biến cho 5 số thực 
 * s2: tạo biến tổng 5 số thực 
 * s3: tạo biến giá trị trung bình của 5 số thực
 * s4: Giá trị TB của 5 số = tổng 5 số chia 5;
 * Output: Giá trị Tb của 5 số thực
*/
console.log('Bài 2:');
var num1 = 1;
var num2 = 2;
var num3 = 3;
var num4 = 4;
var num5 = 5;
var sum_num = num1 + num2 + num3 + num4 + num4;
var TB = sum_num/5;
console.log('Giá trị trung bình của 5 số là: ' + TB)    ;

/**Bài 3
 * Input: Số tiền USD
 * 
 * Step: 
 * s1: tạo biến tỉ lệ quy đổi 1 USD = 23.000 VNĐ
 * s2: tạo biến số tiền USD
 * s3: Tiền Việt = TIến USD X tỉ lệ quy đổi
 * 
 * Output: Tiền Việt được quy đổi
*/

console.log('Bài 3:');

var ratio = 23000;
var USD = 5 ;
var VND = USD*ratio;
console.log('Tiền Việt sau khi quy đổi: ' + VND + ' VNĐ');


/**Bài 4
 * Input: chiều dài, chiều rộng của hình chữ nhật
 * 
 * Step: 
 * s1: tạo biến cho chiều dài, chiều rộng
 * s2: tạo biến cho diện tích, chu vi
 * s3: diện tích = dài X rộng
 * s4: chu vi = (dài + rộng) X 2
 * 
 * Output: Diện tích và chu vi hình chữ nhật
*/

console.log('Bài 4:');

var _length = 5;
var _width = 4;
var area = _length * _width ;
var perimeter = (_length+_width)*2;
console.log('Diện tích HCN: ' + area);
console.log('Chu vi HCN: ' + perimeter);


/**Bài 5
 * Input: Một số có hai chữ số
 * 
 * Step: 
 * s1: tạo biến cho số người dùng nhập
 * s2: tạo biến cho chữ số hàng chục = số người dùng nhập/10
 * s3: tạo biến cho chữ số hàng đơn vị = số người dùng nhập % 10 (chia lấy dư)
 * s4: làm tròn ký số đầu sau đó gán lại giá trị
 * s6: Tổng 2 kí số = chữ số hàng chục + chữ số hàng đơn vị
 * 
 * Output: Diện tích và chu vi hình chữ nhật
*/
console.log('Bài 5:');
var _number = 69;
var first_index = _number / 10;
var second_index = _number % 10;
first_index = parseInt(first_index);
var sum_of_index = first_index + second_index;
console.log('Tổng 2 kí số là: ' + sum_of_index);

